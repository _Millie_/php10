-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 17, 2018 at 04:59 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oglasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `name_product` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL,
  `picture` char(40) NOT NULL,
  `price` float NOT NULL,
  `date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ID`, `name_product`, `description`, `picture`, `price`, `date`, `user_id`) VALUES
(1, 'Rubik’s 5x5 Cube', 'The Rubik’s ‘Professor’s Cube’ is the 5x5x5 version of the famous Rubik’s Cube. The Rubik’s 5x5 is not for the feint hearted and is the most complex of all the Rubik’s Cubes.', 'rubiks_cube1.png', 27.99, '2018-04-18', 1),
(2, 'Rubik’s Junior Bunny', 'This is the perfect gift for introducing youngters into the world of Rubik’s. These fun animal puzzles help develop logical thinking and problem solving skills.', 'rubiks_bunny.png', 9.99, '2018-04-18', 2),
(3, 'Rubik’s Junior Kitten', 'Twist the blocks to create a crazy mixed-up animal, but with relatively few combinations.', 'rubiks_kitten.png', 9.99, '2018-04-25', 2),
(4, 'Rubik’s Magic', 'Rubik’s Magic Puzzle was designed with illusion in mind. By folding the 8 connected square panels in different directions and opening them up again… find out whether you can link the 3 floating rings together. Its design of interconnected panels causes the challenge to solve the floating rings or create a variety of 3D shapes.', 'rubiks_magic.png', 12.49, '2018-04-18', 2),
(5, 'Rubik’s 3x3 Cube', 'This is the new Rubik’s Cube with tough tiles. The traditional stickers have been replaced with plastic; which mean no fading, peeling or cheating! It also has a brand new mechanism that results in a smoother, faster and more reliable Rubik’s Cube. If anyone ever tells you they solve the Rubik’s Cube by peeling the stickers off – hand them one of these! ', 'rubiks_cube2.png', 11.99, '2018-04-17', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(5) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `password` varchar(56) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `active`, `username`, `password`) VALUES
(1, 1, 'user1', '7b625a0e35d697bf657428f846ae6a40'),
(2, 1, 'user2', '11e4dee8c8e12af18b4af17e8717295e'),
(3, 1, 'user3', 'test');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
